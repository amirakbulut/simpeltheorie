<?php

namespace App\Http\Controllers\auth\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function index()
    {
        return view('auth.admin.index');
    }

    public function login(Request $request)
    {
        // validate form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $credentials = $request->only('email', 'password');
        $remember = $request->remember;

        // Attempt to log admin in
        if(Auth::guard('admin')->attempt($credentials, $remember)){
            // if succesful
            return redirect()->intended(route('admin.dashboard'));
        }

        // if not unsuccesful
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    /**
     * Log the user out of the application.
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }
}
