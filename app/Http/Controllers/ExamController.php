<?php

namespace App\Http\Controllers;

use App\Exam;
use Illuminate\Http\Request;
use App\Http\Resources\Exam as ExamResource;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all Exames

        $exams = Exam::all();
        return ExamResource::collection($exams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return create view
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // storing an exam

        $exam = new Exam;
        $exam->id = $request->input('id');
        $exam->title = $request->input('title');
        if($exam->save()) {
            return new ExamResource($exam);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get single Exam
        $exam = Exam::findOrFail($id);
        return new ExamResource($exam);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        // return edit view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        $exam = Exam::findOrFail($request->id);
        $exam->id = $request->input('id');
        $exam->title = $request->input('title');
        if($exam->save()) {
            return new ExamResource($exam);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete exam
        $exam = Exam::findOrFail($id);
        if($exam->delete()) {
            return new ExamResource($exam);
        }
    }

    public function process($quiz_id){
     return 'Quiz met id: ' . $quiz_id . 'word bijgewerkt';
    }
}
