<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function exam() {
        return $this->belongsTo(Exam::class);
    }

    public function choices() {
        return $this->hasMany(Choice::class);
    }
}
