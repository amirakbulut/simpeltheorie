<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/user', function(Request $request){
        return $request->user();
    });

    Route::resource('exams', 'ExamController');
    Route::post('exams/{id}/process', 'ExamController@process');
});

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Exams api
Route::get('exams', 'ExamController@index');
Route::get('exam/{id}', 'ExamController@show');
Route::post('exam', 'ExamController@store');
Route::put('exam', 'ExamController@store');
Route::delete('exam/{id}', 'ExamController@destroy');

*/
