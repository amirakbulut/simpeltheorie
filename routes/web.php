<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('index');
});

Auth::routes();
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/logout', 'Auth\LoginController@userLogout')->name('user.logout');

/* Admin routes */
Route::prefix('cms')->group(function(){
    Route::get('/dashboard', 'Auth\Admin\AdminController@dashboard')->name('admin.dashboard');
    Route::get('/', 'Auth\Admin\LoginController@index')->name('admin.login');
    Route::post('/', 'Auth\Admin\LoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\Admin\LoginController@logout')->name('admin.logout');
});




















Route::get('/page', 'HomeController@index')->name('dashboard');


/* For Postman usage */
Route::get('/token', function () {
    return Auth::user()->createToken('test');
});
