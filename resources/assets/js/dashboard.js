require('jquery-slimscroll');
require('jquery.uniform');
require('switchery');
require('d3');
require('nvd3');
require('jquery.flot');
require('chart.js');
require('./dashboard/space.min');



import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

let Index = require('./components/index');
let Page = require('./components/page');

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/dashboard',
            name: 'index',
            component: Index
        },
        {
            path: '/page',
            name: 'page',
            component: Page
        }
    ]
});
//router.replace({name: 'index'});

const app = new Vue({
    el: '#app',
    components: { Index },
    router
});


