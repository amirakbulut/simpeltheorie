@extends('layouts.index')

@section('content')

    <!-- Register -->
    <main class="signup">
        <div class="container">
            <section class="text-block mb-90">
                <h4 class="text-center">Account aanmaken</h4>
                <p class="text-center border-decor bd-center">Use code <span>BLACKFRIDAY16</span> on your next purchase to save 20%.</p>
            </section>
            <section class="login-form text-center">
                <div class="alert alert-danger display-error" style="display: none"></div>
                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" class="mb-10">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <!-- <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control achternaam_input" placeholder="Achternaam" required="">
                        </div> -->
                        <div class="col-md-6 col-sm-6">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control examendatum_input" onfocus="(this.type='date')" placeholder="Examen datum" required="">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="ipt-btn-block">
                                <input type="number" class="form-control school_input" placeholder="Rijschoolnummer (niet verplicht)">
                                <input type="submit" class="btn btn-small register_submit" value="{{ __('Register') }}">
                            </div>
                        </div>
                    </div>
                </form>
                <a href="#" class="font-16 weight-lighter color-theme-hover">By signing up you agree to out <span class="color-dark">Terms of Service.</span></a>
            </section>
        </div>
    </main>

@endsection
