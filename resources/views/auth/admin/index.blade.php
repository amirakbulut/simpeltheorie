@extends('layouts.index')

@section('content')

    <!-- LOGIN -->
    <main class="login">
        <div class="container">
            <section class="text-block mb-90">
                <h4 class="text-center">{{ __('Login') }} Admin <span class="color-theme">CMS</span></h4>
                <p class="text-center border-decor bd-center">Log in met je e-mailadres en wachtwoord.</p>
            </section>
            <section class="login-form text-center">
                <div class="alert alert-danger display-error" style="display: none"></div>
                <form method="POST" action="{{ route('admin.login.submit') }}" aria-label="{{ __('Login') }}" class="mb-40">
                    @csrf
                    <input id="email" type="email"  class="form-control email_input {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif

                    <div class="ipt-btn-block">
                        <input id="password" type="password"  class="form-control password_input {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                        <input type="submit" class="btn btn-small login_submit" value="{{ __('Login') }}">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>


                </form>
                <a href="{{ route('password.request') }}" class="font-16 weight-lighter color-theme-hover">{{ __('Forgot Your Password?') }}</a>
            </section>
        </div>
    </main>

@endsection
