<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Simpel Theorie">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Dashboard</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->



    <link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico"/>
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Theorie direct - Online leren voor je theorie examen</title>

    <!-- Styles -->
    <link href="{{ asset('fonts/dashboard/Ubuntu.css') }}" rel="stylesheet">
    <!-- Styles -->


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Theme Styles -->
    <link href="{{ asset('css/dashboard/app.css') }}" rel="stylesheet">


</head>
<body  class="page-sidebar-fixed">

<!-- Page Container -->
        @yield('content')
<!-- /Page Container -->

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/dashboard/dashboard.js') }}" defer></script>
</body>
</html>
