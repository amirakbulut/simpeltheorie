let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/* Compile global files */
mix.js('resources/assets/js/app.js', 'public/js');
mix.sass('resources/assets/sass/app.scss', 'public/css');

/* Compile index files */
mix.js('resources/assets/js/index.js', 'public/js/index');
mix.styles([
    'resources/assets/css/index/index.css',
], 'public/css/index/app.css');


/* Compile dashboard files */
mix.js('resources/assets/js/dashboard.js', 'public/js/dashboard');
mix.styles([
    'resources/assets/css/dashboard/icons.css',
    'resources/assets/css/dashboard/default.css',
    'resources/assets/css/dashboard/space.min.css',
    'resources/assets/css/dashboard/admin3.css',
    'resources/assets/css/dashboard/custom.css',
], 'public/css/dashboard/app.css');
